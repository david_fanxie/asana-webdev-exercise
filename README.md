# PetSana

## How to see it
All CSS and JS is already compiled, just open the `index.html` file with any browser :)

## Tech used

#### SASS/SCSS
I chose SCSS because it would allow me to have a better structure, which in time allows for better **maintainability**.
My `.scss` files are split among a few folders (or layers), most files are relatively small, with a few lines only.

I'm importing part of a CSS framework I built so I could have a simple flexbox-based grid and a container. These imports can be found in `src/scss/03-Generic/vendors.scss`

#### JS/REACT
There were several ways to parse the json data file and display the pictures.

I chose **React** because it allowed me to separate concerns in different components. Plus, templating is quite easy and fast with JSX.

#### Build tools - Laravel Mix
To compile the assets I chose Laravel Mix, which is built on top of webpack.
It's easy to use, it allowed me to get started quickly and it adapts to pretty much every frontend workflow.

## Observations

The site should have a way to filter between cats and dogs, and some sort of pagination.
Unfortunately I didn't have enough time to work on this.

I chose to separate the service that reads the json data file from the `Pets` component. I did it this way because this allows for scalability, if, for example, we start loading the animals from an API.

With that said, I'd like to be evaluated with a Design focus, though I feel comfortable with my build tools as well.

Original README below.

-------------------------------------------------------------------------------------------------------

# Asana WebDev Take-Home Exercise

## Instructions

Let's pretend that Asana is actually a pet adoption agency. (Our SF office is so dog-friendly that it's not too far a stretch!) We need a web page that contains an image gallery of the pets we have available for adoption. We'd like you to build it.

To help you get started, we've included a few pre-built frontend components, a data file that you should use to populate the gallery, notes on intended user interactions, and guidance about how this gallery supports our efforts to get more pets adopted.

**What to submit:** We'd like to see your incremental work along with finished product, so please either ZIP and submit your version-controlled directory, or link us to your hosted repository.

**What technologies to use:** We'd like you to pick technologies that help us get clear signal on your frontend fundamentals. For that reason, we recommend that you stick with vanilla JS, CSS, and HTML -- but if there's a framework, pre- or post-processor, templating language, or build tooling that you think would make for a stronger submission, go for it.

We'll be grading on five areas:
- **Fidelity -** How well does it work? Does it do what the design calls for?
- **Robustness -** Does the page work across modern web browsers on desktop and mobile?
- **Maintainability -** Is the project written cleanly, organized clearly and documented well?
- **Performance -** Is the page built to load fast and rank well in search engines?
- **Your choice!** Let us know which one (just one!) of these criteria we should use to evaluate your submission:
  - **Design Focus:** How slick and polished are the page's UI and UX?
  - **Growth Focus:** How did the questions from the team trying to boost adoption influence your technical choices?
  - **Tooling Focus:** How did your use of build tools help you ship something high-quality faster?

## Guidance from the design team

"These images should be displayed on the page as a set of thumbnails; clicking on a thumbnail should display a full-sized image to the user."

"Full-size photos should be able to be closed, and the user should be returned to the thumbnail gallery."

## Requirements from the adoption team

"Starting next month, we'll be partnering with a shelter that has 2000 pets available for adoption. We need this page to be able to handle that many listings."

"Most people find out about us from internet searches. We need this page to appear towards the top of search results."
