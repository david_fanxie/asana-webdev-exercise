'use strict';

const e = React.createElement;

class SiteMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showMenu: false
        };
    }

    toggleMenu() {
        this.setState({
            showMenu: !this.state.showMenu
        });
    }

    render() {
        return (
            <div className="site-header-nav-wrapper">
                <nav className={"site-header-nav" + (this.state.showMenu ? ' is-open' : '')}>
                    <a href="/">About Us</a>
                </nav>
                <div className="site-header-mobile">
                    <a href="javascript:" className={"nav-icon"  + (this.state.showMenu ? ' is-open' : '')} onClick={this.toggleMenu.bind(this)}>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
            </div>
        )
    }

}

class SiteHeader extends React.Component {

    render() {
        return (
            <header className="site-header">
                <div className="site-header-brand">
                    <a href="/" className="site-header-logo">PetSana</a>
                </div>
                <SiteMenu/>
            </header>
        );
    }

}

class SiteQuote extends React.Component {

    render() {
        return (
            <section className="section section-quote">
                <div className="container">
                    <h1 className="title">Puppy image gallery</h1>

                    <blockquote className="header-quote">
                        <p>A dog will teach you unconditional love. If you can have that in your life, things won't be too
                            bad.</p>

                        <cite>&mdash;Robert Wagner</cite>
                    </blockquote>
                </div>
            </section>
        )
    }
}

class PetsContainer extends React.Component {

    render() {
        return (
            <section className="section">

            </section>
        )
    }

}

class Site extends React.Component {

    render() {
        return (
            <div>
                <SiteHeader/>
                <SiteQuote/>
                <PetsContainer/>
            </div>
        )
    }

}

const rootContainer = document.querySelector('#root');
ReactDOM.render(<Site/>, rootContainer);