'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

//  Components
import SiteHeader from './components/SiteHeader/SiteHeader';
import SiteQuote from './components/SiteQuote/SiteQuote'
import Pets from './components/Pets/Pets';


class Site extends React.Component {

    render() {
        return (
            <div>
                <SiteHeader/>
                <SiteQuote/>
                <Pets/>
            </div>
        )
    }

}

const rootContainer = document.querySelector('#root');
ReactDOM.render(<Site/>, rootContainer);
