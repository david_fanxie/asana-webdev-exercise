import React, { Component } from 'react';

class SiteQuote extends Component {

    render() {
        return (
            <section className="section section-quote">
                <div className="container">
                    <h1 className="title">Puppy and kittens image gallery</h1>

                    <blockquote className="header-quote">
                        <p>A dog will teach you unconditional love. If you can have that in your life, things won't be too
                            bad.</p>

                        <cite>&mdash;Robert Wagner</cite>
                    </blockquote>
                </div>
            </section>
        )
    }
}

export default SiteQuote;
