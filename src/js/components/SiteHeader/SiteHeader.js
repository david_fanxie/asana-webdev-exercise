import React, { Component } from 'react';
import SiteMenu from './SiteMenu';

class SiteHeader extends Component {

    render() {
        return (
            <header className="site-header">
                <div className="site-header-brand">
                    <a href="/" className="site-header-logo">PetSana</a>
                </div>
                <SiteMenu/>
            </header>
        );
    }

}

export default SiteHeader;
