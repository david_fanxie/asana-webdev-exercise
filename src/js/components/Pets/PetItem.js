import React, { Component } from 'react';

class PetItem extends Component {

    showPetDetails() {
        this.props.onPetClick();
    }

    render() {
        return (
            <article className="pet-item">
                <a href="javascript:" onClick={this.showPetDetails.bind(this)}>
                    <figure className="pet-item-thumb">
                        <img src={"." + this.props.pet.image} alt={"Adopt " + this.props.pet.name}/>
                    </figure>
                    <span className="pet-item-title">
                        <h3 className="title">{ this.props.pet.name }</h3>
                    </span>
                </a>
            </article>
        )
    }
}

export default PetItem;
