import React, { Component } from 'react';
import PetItem from './PetItem';
import PetModal from './PetModal';

//  Services
import PetServices from '../../services/PetServices';

class Pets extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pets: [],
            showModal: false,
            modalPet: null
        };
    }

    componentWillMount() {

        PetServices.getPets().then(pets => {
            this.setState({
                pets: pets
            });
        })
    }

    showModal( pet ) {
        this.setState({
            showModal: true,
            modalPet: pet
        });
    };

    hideModal() {
        this.setState({
            showModal: false,
            modalPet: null
        });
    };

    render() {
        return (
            <section className="section" id="pets-wrapper">
                <PetModal show={this.state.showModal} pet={this.state.modalPet} handleClose={this.hideModal.bind(this)}/>
                <div className="container">
                    <div className="columns">
                        {
                            this.state.pets.map(( pet, i ) => {
                                return (
                                    <div className="column is-6-tablet is-3-desktop" key={i}>
                                        <PetItem pet={pet} onPetClick={() => { this.showModal(pet) }} />
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </section>
        )
    }

}

export default Pets;
