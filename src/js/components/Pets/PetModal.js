import React, { Component } from 'react';

class PetModal extends Component {

    render() {
        return this.props.show && this.props.pet ? (
            <div className="modal">
                <article className="modal-card">
                    <header className="modal-card-header">
                        <h2 className="title">{this.props.pet.name}</h2>
                        <button className="close-button" onClick={this.props.handleClose}><span className="delete"></span></button>
                    </header>
                    <main className="modal-card-content">
                        <figure className="modal-content-thumb">
                            <img src={"." + this.props.pet.image} alt={this.props.pet.name}/>
                        </figure>
                        <section className="modal-content-details">
                            <div className="modal-content-text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aspernatur consequatur doloremque ex in inventore iste itaque labore modi necessitatibus perferendis praesentium quae quam quas sapiente, sint tempora. At commodi culpa cumque, dolorum eligendi explicabo facilis illo iure nulla, quibusdam quidem ratione unde vel vitae, voluptas. Dolor magnam molestias similique?</p>
                            </div>
                            <div className="modal-content-actions">
                                <button className="button">Take me home</button>
                            </div>
                        </section>
                    </main>
                </article>
            </div>
        ) : null
    }

}

export default PetModal;
