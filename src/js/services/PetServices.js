import dogs from '../../../assets/data/dogs.json';
import cats from '../../../assets/data/cats.json';

const shuffleArray = arr => arr
    .map(a => [Math.random(), a])
    .sort((a, b) => a[0] - b[0])
    .map(a => a[1]);

export default {

    getPets( params = {} ) {

        //  In a real life scenario queries would be made to the server asynchronously, so let's simulate that
        return new Promise( (resolve, reject) => {
            setTimeout(() => {
                const allDogs = dogs.dogs;
                const allCats = cats.cats;

                const pets = allDogs.concat( allCats );

                //  Shuffle array so it looks more interesting
                resolve( shuffleArray( pets ) );
            }, 300)
        } );
    }
}
